# Booster iOS

## 2019-20-08
1. Product Name 产品名称:               Booster  

2. Organization Name 组织名字:          Booster  

3. Organization Identifier 组织标示:    com.Booster  
   因为 SwiftUI 只支持 iOS 13 以上，所以还是要选择 Storyboard 的布局方式

4. Bundle Identifier 产品标示:          com.Booster.Booster  
   组织标示.产品名称，用于唯一标示一个 app ， 如果相同，后面安装的会覆盖之前安装的
   以后会增加 product name：             HongXing   FeiFan   LanHai   JuFeng
   
5. 添加 .gitignore 文件，创建不了这个文件，下载 
   [gitlab.com 的模版项目里的 raw 文件](https://gitlab.com/gitlab-org/project-templates/iosswift/raw/master/.gitignore)
   
   下载下来是隐藏文件，需要按下 Command + Shift + . 来显示或隐藏，  
   [.gitignore 更新为 github 的内容](https://raw.githubusercontent.com/github/gitignore/master/Swift.gitignore)
   
6. 初始化 cocoapods：  
   安装好 cocoapods ，终端进入本目录，执行 pod init ，  
   会产生 Podfile ，修改第二行，platform :ios, '8.0'
   支持 8.0 以上，  
   终端继续执行 pod install  
   就有 .xcworkspace 和 pods 目录，  
   修改 .gitignore 文件，添加一行 Pods/ 和 Podfile.lock ，最后一行 .idea/  
   
7. 用 AppCode 打开本项目，会提示有这个 .xcworkspace 文件，就按提示打开即可
   还微添加什么，可以直接在模拟器运行，没问题就好  

7. 以后可以 cocoapods 添加 coobjc 这个协程库